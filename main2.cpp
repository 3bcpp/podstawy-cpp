#include <iostream>

int main()
{
    //najprostsza zmienna - zajmuje 1 bajt (8 bit)
    //przechowuje wartość liczbową wskazanego znaku
    //kompilator wie, że wartość tą ma wyświetlać jako znak
    char znak='A';
    /*zajmuje 1 bajt; też jest liczbą
      wartość 0 to zawsze false (fałsz)
      wartość inne zawsze true (prawda)
    */
    bool zlaPogoda=0; //najczęściej i tak stosuje się wartość true/false zamiast liczb
    /* Zmienna przechowuje liczby całkowite
     * Długość zmiennej wynosi 16 bit (2 bajty)
     *
     * od -32768 do 32767
     *
     * gdyby zmienna była bezznakowa
     * od 0 do 65535
     * */
    short liczba=15;
    /* Zmienna przechowuje liczby całkowite
     * co najmniej 16 bitowe (2 bajty)
     * Rzeczywisty rozmiar zależy od platformy
     * (przeważnie 32 bity, czasami 64).
     *
     * Najczęściej są tożsame z teoretycznym typem o nazwie
     * słowo (word), które określa wielkość bitową danej
     * platformy
     * */
    int calkowita= 134;
    /* Zmienna przechowuje liczby całkowite
     * 2x większe niż int (jednak nie mniejsze
     * od niego)
     * */
    long dluga=16789;
    /* Przechowuje liczby całkowite 2x większe niż long
     * (jednak nie mniejsze od niego)
     * */
    long long bdluga=1244553;
    /* Zmienna zmiennoprzecinkowa (ułamkowa); tzw.
     * pojedynczej precyzji; zajmuje co najmniej 16 bit
     * (przeważnie 32), z czego 20 bit przeznaczane jest
     * na część ułamkową
     *
     * Ważne jest by na końcu wartości zaznaczyć, iż jest to
     * float (litera f)
     * */
    float zmienna=1.45f;
    /*Zmienna zmiennoprzecinkowa podwójnej precyzji
     * posiada dwa razy więcej miesc na część ułamkową
     * niż float
     * */
    double podwojna=6.78;
    std::cout <<"Program wyswietla informacje o wielkosciach (bajtach)\n poszczegolnych zmiennych na naszej platformie\n";
    std::cout << "Typ\t\t\tWartosc\t\t\tRozmiar (sizeof)";
    std::cout << "\n------------------------------------------------------------------";
    std::cout << "\nchar\t\t\t"<< znak << "\t\t\t" << sizeof(znak) << '\n';
    std::cout << "\nbool\t\t\t"<< zlaPogoda << "\t\t\t" << sizeof(zlaPogoda) << '\n';
    std::cout << "\nshort\t\t\t"<< liczba << "\t\t\t" << sizeof(liczba) << '\n';
    std::cout << "\nint\t\t\t"<< calkowita << "\t\t\t" << sizeof(calkowita) << '\n';
    std::cout << "\nlong\t\t\t"<< dluga << "\t\t\t" << sizeof(dluga) << '\n';
    std::cout << "\nlong long\t\t\t"<< bdluga << "\t\t\t" << sizeof(bdluga) << '\n';
    std::cout << "\nfloat\t\t\t"<< zmienna << "\t\t\t" << sizeof(zmienna) << '\n';
    std::cout << "\ndouble\t\t\t"<< podwojna << "\t\t\t" << sizeof(podwojna) << '\n';
    return 0;
}
