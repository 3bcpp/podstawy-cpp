/* To jest przykład komentarza wielolinowego
   Można go stosować naprzemiennie z komentarzami jednoliniowymi
   Zawsze kończy się symbolem */
/* Komentarz ten zawsze kończy się po wspomnainej sekwencji
 * więc nalezy stawiać ją zawsze i bezwzględnie na końcu
 * komentarza, inaczej reszta komentarza może zostać
 * nieumyślnie potraktowana jako kod aplikacji
 *
 * Gwiazdki dodane z lewej strony są czysto dekoracyjnie,
 * mogą być dodawane ręcznie lub przez środowisko programistyczne
 *
 * Komentarze mogą także służyć jako czasowe wyłączenie kodu
 * przykład w kodzie
*/
#include <iostream>

int main()
{
    //Przykład wyświetlania wyjścia programu; ten tekst nie zostanie wzięty pod uwagę w czasie kompilacji
    //Komentarz ten jest jednoliniowy - rozpoczyna się od // i kończy na ostatnim znaku w linii

    std::cout << "Wynik (2+2)*2= " << (2+2)*2 << "\rPierwszy"
              << "\nJeszcze jeden tekst" << std::endl <<
                 " i jeszcze jeden" << std::endl;
    //std::cout << "Ta linia powinna byc niewidoczna - jest w komentarzu!\n";

    std::cout <<"Tekst widoczny" /*<<"Tekst w komentarzu"*/ << std::endl;
    return 0;
}
